package main.TP;

import main.Node;
import main.SOURCE.ParcoursUtils;

import java.util.ArrayList;
import java.util.List;

public class Tp1 {

    public static void main(String[] args) {

        // TP 1 - exercice 1

        Node<String> parentNode = new Node<String>("A");
        Node<String> nodeB = new Node<String>("B", parentNode);
        Node<String> nodeC = new Node<String>("C", parentNode);
        Node<String> nodeD = new Node<String>("D", parentNode);
        Node<String> nodeE = new Node<String>("E");
        Node<String> nodeF = new Node<String>("F");
        Node<String> nodeG = new Node<String>("G");
        Node<String> nodeH = new Node<String>("H");
        Node<String> nodeI = new Node<String>("I");
        Node<String> nodeJ = new Node<String>("J");
        Node<String> nodeK = new Node<String>("K");
        Node<String> nodeL = new Node<String>("L");
        Node<String> nodeM = new Node<String>("M");
        Node<String> nodeN = new Node<String>("N");
        Node<String> nodeO = new Node<String>("O");
        Node<String> nodeP = new Node<String>("P");

        parentNode.addChild(nodeB);
        parentNode.addChild(nodeC);
        parentNode.addChild(nodeD);
        nodeB.addChild(nodeE);
        nodeB.addChild(nodeF);
        nodeE.addChild(nodeJ);
        nodeE.addChild(nodeK);
        nodeE.addChild(nodeL);
        nodeD.addChild(nodeG);
        nodeD.addChild(nodeH);
        nodeD.addChild(nodeI);
        nodeG.addChild(nodeM);
        nodeM.addChild(nodeP);
        nodeI.addChild(nodeN);
        nodeI.addChild(nodeO);

        // TP 1 - exercice 2

        Node<Integer> parentNode20 = new Node<Integer>(20);
        Node<Integer> Node5 = new Node<Integer>(5);
        Node<Integer> Node12 = new Node<Integer>(12);
        Node<Integer> Node3 = new Node<Integer>(3);
        Node<Integer> Node25 = new Node<Integer>(25);
        Node<Integer> Node8 = new Node<Integer>(8);
        Node<Integer> Node6 = new Node<Integer>(6);
        Node<Integer> Node13 = new Node<Integer>(13);
        Node<Integer> Node21 = new Node<Integer>(21);
        Node<Integer> Node28 = new Node<Integer>(28);
        parentNode20.addChild(Node5);
        parentNode20.addChild(Node25);
        Node5.addChild(Node3);
        Node5.addChild(Node12);
        Node12.addChild(Node8);
        Node8.addChild(Node6);
        Node12.addChild(Node13);
        Node25.addChild(Node21);
        Node25.addChild(Node28);

        // Implementation des différents parcours

        System.out.println("-----------------------------   TP1   ---------------------------");
        System.out.println("");
        List<Node<Integer>> resultParcoursLargeur = ParcoursUtils.bfs(parentNode20);
        List<Node<Integer>> resultParcoursProfondeurSuffixe = new ArrayList<>();
        List<Node<Integer>> resultParcoursProfondeurPrefixe = new ArrayList<>();
        List<Node<Integer>> resultParcoursProfondeurInfixe = new ArrayList<>();
        System.out.println("Résultat parcours en largeur " + resultParcoursLargeur.toString());
        System.out.println("-----------------------------------------------------------------");
        System.out.println("Résultat parcours en profondeur suffixe :" +  ParcoursUtils.parcoursProfondeurSuffixe(parentNode20, resultParcoursProfondeurSuffixe).toString());

        System.out.println("-----------------------------------------------------------------");
        System.out.println("Résultat parcours en profondeur prefixe :" +  ParcoursUtils.parcoursProfondeurPrefixe(parentNode20, resultParcoursProfondeurPrefixe).toString());

        System.out.println("-----------------------------------------------------------------");
        System.out.println("Résultat parcours en profondeur infixe :" +  ParcoursUtils.parcoursProfondeurInfixe(parentNode20, resultParcoursProfondeurInfixe).toString());

    }


}
