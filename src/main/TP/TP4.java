package main.TP;

import main.BinaryNode;
import main.Node;
import main.SOURCE.ParcoursUtils;

import java.util.ArrayList;
import java.util.List;

public class TP4 {

    public static void main(String[] args) {

        System.out.println("---------------------  TP4   ------------------");

        // arbre equilibre

        BinaryNode<Integer> node66 = new BinaryNode<Integer>(66);
        BinaryNode<Integer> node67 = new BinaryNode<Integer>(67);
        BinaryNode<Integer> node68 = new BinaryNode<Integer>(68);

        node66.setFeuilleGauche(node67);
        node66.setFeuilleDroite(node68);

        System.out.println("Resultat de la méthode isEquilibre : " + ParcoursUtils.isEquilibre(node66));
        System.out.println("Resultat du facteur d'equilibrage : " + ParcoursUtils.getFacteurEquilibrage(node66));


        //Rotation
        BinaryNode<Integer> racine = new BinaryNode<>(3);
        BinaryNode<Integer> node5 = new BinaryNode<>(5);
        BinaryNode<Integer> node7 = new BinaryNode<>(7);
        racine.setFeuilleDroite(node5);
        node5.setFeuilleDroite(node7);

        BinaryNode<Integer> nodeWithRotation = ParcoursUtils.rotationGauche(racine);
        System.out.println("Rotation gauche de l'abre ---->");
        System.out.println("Racine après rotation : " + nodeWithRotation.data);
        System.out.println("Feuille gauche après rotation : " + nodeWithRotation.feuilleGauche.data);
        System.out.println("Feuille droite après rotation : " + nodeWithRotation.feuilleDroite.data);


    }
}
