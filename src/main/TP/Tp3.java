package main.TP;

import main.BinaryNode;
import main.SOURCE.ParcoursUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Tp3 {

    public static void main(String[] args) {

        // Création et implémentation d'un arbre binaire

        // Création de l'abre binaire à la main
        BinaryNode<Integer> node20 = new BinaryNode<Integer>(20);
        BinaryNode<Integer> node5 = new BinaryNode<Integer>(5);
        BinaryNode<Integer> node3 = new BinaryNode<Integer>(3);
        BinaryNode<Integer> node12  = new BinaryNode<Integer>(12);
        BinaryNode<Integer> node8  = new BinaryNode<Integer>(8);
        BinaryNode<Integer> node6  = new BinaryNode<Integer>(6);
        BinaryNode<Integer> node13  = new BinaryNode<Integer>(13);
        BinaryNode<Integer> node25  = new BinaryNode<Integer>(25);
        BinaryNode<Integer> node21  = new BinaryNode<Integer>(21);
        BinaryNode<Integer> node28  = new BinaryNode<Integer>(28);

        // Implémentation de l'abre à la main
        node20.setFeuilleDroite(node25);
        node20.setFeuilleGauche(node5);
        node5.setFeuilleGauche(node3);
        node5.setFeuilleDroite(node12);
        node12.setFeuilleGauche(node8);
        node12.setFeuilleDroite(node13);
        node8.setFeuilleGauche(node6);
        node25.setFeuilleGauche(node21);
        node25.setFeuilleDroite(node28);

        // deuxième création de l'abre avec une méthode d'insertion
        BinaryNode<Integer> root = new BinaryNode<Integer>(20);
        root.insert(root,5);
        root.insert(root,3);
        root.insert(root,12);
        root.insert(root,8);
        root.insert(root,6);
        root.insert(root,13);
        root.insert(root,25);
        root.insert(root,21);
        root.insert(root,28);






        // Parcours infixe de l'abre de binaire
        List<BinaryNode<Integer>> resultParcoursInfixe =  new ArrayList<>();
        System.out.println("---------------------  TP3   ------------------");
        System.out.println("---------------------  Exercice 1   ------------------");
        System.out.println("Parcours infixe d'abre binaire géneré à la main : ");
        ParcoursUtils.parcoursInfixeBinaryNode(node20, true);
        System.out.println();
        System.out.println("Parcours infixe d'abre binaire géneré avec la méthode insert : ");
        ParcoursUtils.parcoursInfixeBinaryNode(root, true);
        System.out.println();
        System.out.println("---------------------  Exercice 2   ------------------");

        // Création d'une liste aléatoire de 10 000 valeurs
        List<Integer> list = new ArrayList<>();

        for (int i = 0; i < 10000; i++) {
            int randInt = ThreadLocalRandom.current().nextInt(0, 10000);
            list.add(randInt);
        }

        // Création de l'abre aléatoire
        BinaryNode<Integer> randomTree = new BinaryNode<Integer>(0);
        randomTree.createFromList(randomTree, list);

        // Comparaison de recherche
        List<Integer> randomSearched = new ArrayList<>();

        for (int i = 0; i < 1000; i++) {
            int randInt = ThreadLocalRandom.current().nextInt(0, 10000);
            randomSearched.add(randInt);
        }

        long startTime = System.nanoTime();

        for (Integer valueSearched: randomSearched) {
           BinaryNode<Integer> result = randomTree.search(randomTree, valueSearched);
        }

        System.out.println("Temps d'execution de la méthode basé sur un arbre binaire : " +  (System.nanoTime() - startTime)  + " ns");

        List<Integer> valueFound = new ArrayList<>();

        long startTimeClassicMethod = System.nanoTime();
        for (Integer valueInList : list) {
            for (Integer valueSearched : randomSearched) {
                if (valueInList == valueSearched) {
                    valueFound.add(valueInList);
                }
            }
        }

        System.out.println("Temps d'execution de la méthode classique : " +  (System.nanoTime() - startTimeClassicMethod)  + " ns");


        long startTimeSortCollections = System.nanoTime();
        Collections.sort(list);
        System.out.println("Temps d'execution du tri via la méthode de Collection : " +  (System.nanoTime() - startTimeSortCollections)  + " ns");

        long startTimeSortBinaryThree = System.nanoTime();
        ParcoursUtils.parcoursInfixeBinaryNode(randomTree, false);
        System.out.println("Temps d'execution du tri via la méthode de la recherche par profondeur infixe : " +  (System.nanoTime() - startTimeSortBinaryThree)  + " ns");

        System.out.println("On remarque que le tri ainsi que la recherche dans un abre binaire sont plus rapides car " +
                "il n'est pas nécessaire de parcourir toute la liste pour trouve le résultat attendu");









    }
}
