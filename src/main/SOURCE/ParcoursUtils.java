package main.SOURCE;

import main.BinaryNode;
import main.Node;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class ParcoursUtils {

    public static Queue<Node> queue = new LinkedList<Node>();


    // Recherche par largeur
     public static List<Node<Integer>> bfs(Node node) {
        queue.add(node);
        List<Node<Integer>> result = new ArrayList<Node<Integer>>();
        while (!queue.isEmpty()) {
            Node<Integer> currentNode = queue.remove();
            result.add(currentNode);
            for (Node<Integer> children : currentNode.children) {
                queue.add(children);
            }

        }

        return result;

    }

    public static <T> Boolean isEquilibre(BinaryNode<T> tree) {
        if (tree.getFeuilleDroite() != null) {
            if(getFacteurEquilibrage(tree.getFeuilleDroite()) != 0 && getFacteurEquilibrage(tree.getFeuilleDroite()) != 1 ) {
                return false;
            }
            isEquilibre(tree.feuilleDroite);
        }

        if (tree.getFeuilleGauche() != null) {
            if(getFacteurEquilibrage(tree.getFeuilleGauche()) != 0 && getFacteurEquilibrage(tree.getFeuilleGauche()) != 1 ) {
                return false;
            }
            isEquilibre(tree.feuilleGauche);
        }

        return true;

    }

    public static <T> Integer getFacteurEquilibrage(BinaryNode<T> tree) {
        return Math.abs(getProfondeur(tree.feuilleGauche) - getProfondeur(tree.getFeuilleDroite()));
    }

    public static <T> Integer getProfondeur(BinaryNode<T> tree) {
        if (tree == null) {
            return 0;
        } else {
            return 1 + getMax(getProfondeur(tree.getFeuilleGauche()), getProfondeur(tree.feuilleDroite));
        }
    }

    public static Integer getMax(Integer a, Integer b) {
        return (a>b) ? a : b;
    }

    // Recherche par profondeur préfixe
    public static List<Node<Integer>> parcoursProfondeurPrefixe(Node<Integer> origine, List<Node<Integer>> result) {
        result.add(origine);
        for (Node<Integer> children : origine.getChildren()) {
            parcoursProfondeurPrefixe(children, result);
        }
        return result;
    }

    // Recherche par profondeur préfixe
    public static List<Node<Integer>> parcoursProfondeurSuffixe(Node<Integer> origine, List<Node<Integer>> result) {

        for (Node<Integer> children : origine.getChildren()) {
            parcoursProfondeurSuffixe(children, result);
        }
        result.add(origine);
        return result;
    }


    // Recherche par profondeur infixe
    public static List<Node<Integer>> parcoursProfondeurInfixe(Node<Integer> origine, List<Node<Integer>> result) {

        for (Node<Integer> children : origine.getChildren()) {
            parcoursProfondeurSuffixe(children, result);
        }
        result.add(origine);
        return result;
    }

    public static void parcoursInfixeBinaryNode(BinaryNode<Integer> node, boolean display) {

        if (node.getFeuilleGauche() != null) {
            parcoursInfixeBinaryNode(node.getFeuilleGauche(), display);
        }

        if (display) {
            System.out.print(node.data + " ");
        }

        if (node.getFeuilleDroite() != null) {
            parcoursInfixeBinaryNode(node.getFeuilleDroite(), display);
        }

    }

    public static void tableToBinaryThree(List<Integer> table) {
        for (Integer value : table) {
            BinaryNode<Integer> node = new BinaryNode<Integer>(value);


        }
    }

    public static BinaryNode<Integer> rotationGauche(BinaryNode<Integer> node) {
        BinaryNode<Integer> node2 = node.feuilleDroite;
        node.feuilleDroite = node2.feuilleGauche;
        node2.feuilleGauche = node;
        return node2;
    }

}
