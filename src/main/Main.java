package main;

import main.TP.TP4;
import main.TP.Tp1;
import main.TP.Tp3;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Main {

//	public static Queue<Node> queue = new LinkedList<Node>();
//
//	// Recherche par largeur
//	static List<Node<Integer>> bfs(Node node) {
//		queue.add(node);
//		List<Node<Integer>> result = new ArrayList<Node<Integer>>();
//		while (!queue.isEmpty()) {
//			Node<Integer> currentNode = queue.remove();
//			result.add(currentNode);
//			for (Node<Integer> children : currentNode.children) {
//				queue.add(children);
//			}
//
//		}
//
//		return result;
//
//	}
//
//	static <T> Boolean isEquilibre(BinaryNode<T> tree) {
//		if (tree.getFeuilleDroite() != null) {
//			if(getFacteurEquilibrage(tree.getFeuilleDroite()) != 0 && getFacteurEquilibrage(tree.getFeuilleDroite()) != 1 ) {
//				return false;
//			}
//			isEquilibre(tree.feuilleDroite);
//		}
//
//		if (tree.getFeuilleGauche() != null) {
//			if(getFacteurEquilibrage(tree.getFeuilleGauche()) != 0 && getFacteurEquilibrage(tree.getFeuilleGauche()) != 1 ) {
//				return false;
//			}
//			isEquilibre(tree.feuilleGauche);
//		}
//
//		return true;
//
//	}
//
//	static <T> Integer getFacteurEquilibrage(BinaryNode<T> tree) {
//		return Math.abs(getProfondeur(tree.feuilleGauche) - getProfondeur(tree.getFeuilleDroite()));
//	}
//
//	static <T> Integer getProfondeur(BinaryNode<T> tree) {
//		if (tree == null) {
//			return 0;
//		} else {
//			return 1 + getMax(getProfondeur(tree.getFeuilleGauche()), getProfondeur(tree.feuilleDroite));
//		}
//	}
//
//	static Integer getMax(Integer a, Integer b) {
//		return (a>b) ? a : b;
//	}
//
//	// Recherche par profondeur préfixe
//	static List<Node<Integer>> parcoursProfondeurPrefixe(Node<Integer> origine, List<Node<Integer>> result) {
//		result.add(origine);
//		System.out.println(origine.getData());
//
//		for (Node<Integer> children : origine.getChildren()) {
//			parcoursProfondeurPrefixe(children, result);
//		}
//		return result;
//	}
//
//	// Recherche par profondeur préfixe
//		static List<Node<Integer>> parcoursProfondeurSuffixe(Node<Integer> origine, List<Node<Integer>> result) {
//
//			for (Node<Integer> children : origine.getChildren()) {
//				parcoursProfondeurSuffixe(children, result);
//			}
//			result.add(origine);
//			return result;
//		}
//
//
//		// Recherche par profondeur infixe
//			static List<Node<Integer>> parcoursProfondeurInfixe(Node<Integer> origine, List<Node<Integer>> result) {
//
//				for (Node<Integer> children : origine.getChildren()) {
//					parcoursProfondeurSuffixe(children, result);
//				}
//				result.add(origine);
//				return result;
//			}
//
//			static void parcoursInfixeBinaryNode(BinaryNode<Integer> node) {
//
//				if (node.getFeuilleGauche() != null) {
//					parcoursInfixeBinaryNode(node.getFeuilleGauche());
//				}
//
//				System.out.print(node.data + " ");
//
//				if (node.getFeuilleDroite() != null) {
//					parcoursInfixeBinaryNode(node.getFeuilleDroite());
//				}
//
//			}
//
//			static void tableToBinaryThree(List<Integer> table) {
//				for (Integer value : table) {
//					BinaryNode<Integer> node = new BinaryNode<Integer>(value);
//
//
//				}
//			}

			
		

	public static void main(String[] args) {

		Tp1.main(null);
		Tp3.main(null);
		TP4.main(null);

//		// TP 1 - exercice 1
//
//		Node<String> parentNode = new Node<String>("A");
//		Node<String> nodeB = new Node<String>("B", parentNode);
//		Node<String> nodeC = new Node<String>("C", parentNode);
//		Node<String> nodeD = new Node<String>("D", parentNode);
//		Node<String> nodeE = new Node<String>("E");
//		Node<String> nodeF = new Node<String>("F");
//		Node<String> nodeG = new Node<String>("G");
//		Node<String> nodeH = new Node<String>("H");
//		Node<String> nodeI = new Node<String>("I");
//		Node<String> nodeJ = new Node<String>("J");
//		Node<String> nodeK = new Node<String>("K");
//		Node<String> nodeL = new Node<String>("L");
//		Node<String> nodeM = new Node<String>("M");
//		Node<String> nodeN = new Node<String>("N");
//		Node<String> nodeO = new Node<String>("O");
//		Node<String> nodeP = new Node<String>("P");
//
//		parentNode.addChild(nodeB);
//		parentNode.addChild(nodeC);
//		parentNode.addChild(nodeD);
//		nodeB.addChild(nodeE);
//		nodeB.addChild(nodeF);
//		nodeE.addChild(nodeJ);
//		nodeE.addChild(nodeK);
//		nodeE.addChild(nodeL);
//		nodeD.addChild(nodeG);
//		nodeD.addChild(nodeH);
//		nodeD.addChild(nodeI);
//		nodeG.addChild(nodeM);
//		nodeM.addChild(nodeP);
//		nodeI.addChild(nodeN);
//		nodeI.addChild(nodeO);
//
//		// arbre exercices 2
//		/**Node<Integer> parentNode20 = new Node<Integer>(20);
//		Node<Integer> Node5 = new Node<Integer>(5);
//		Node<Integer> Node12 = new Node<Integer>(12);
//		Node<Integer> Node3 = new Node<Integer>(3);
//		Node<Integer> Node25 = new Node<Integer>(25);
//		Node<Integer> Node8 = new Node<Integer>(8);
//		Node<Integer> Node6 = new Node<Integer>(6);
//		Node<Integer> Node13 = new Node<Integer>(13);
//		Node<Integer> Node21 = new Node<Integer>(21);
//		Node<Integer> Node28 = new Node<Integer>(28);
//		parentNode20.addChild(Node5);
//		parentNode20.addChild(Node25);
//		Node5.addChild(Node3);
//		Node5.addChild(Node12);
//		Node12.addChild(Node8);
//		Node8.addChild(Node6);
//		Node12.addChild(Node13);
//		Node25.addChild(Node21);
//		Node25.addChild(Node28);*/
//
//		List<Node<Integer>> result = new ArrayList<Node<Integer>>();
//
//		//parcoursProfondeurPrefixe(parentNode20, result);
//		//result = bfs(parentNode20);
//		/**parcoursProfondeurSuffixe(parentNode20, result);
//		System.out.println("résultat du parcours : " + result.toString());*/
//
//		// Arbre binaire
//
//		BinaryNode<Integer> node20 = new BinaryNode<Integer>(20);
//		BinaryNode<Integer> node5 = new BinaryNode<Integer>(5);
//		BinaryNode<Integer> node3 = new BinaryNode<Integer>(3);
//		BinaryNode<Integer> node12  = new BinaryNode<Integer>(12);
//		BinaryNode<Integer> node8  = new BinaryNode<Integer>(8);
//		BinaryNode<Integer> node6  = new BinaryNode<Integer>(6);
//		BinaryNode<Integer> node13  = new BinaryNode<Integer>(13);
//		BinaryNode<Integer> node25  = new BinaryNode<Integer>(25);
//		BinaryNode<Integer> node21  = new BinaryNode<Integer>(21);
//		BinaryNode<Integer> node28  = new BinaryNode<Integer>(28);
//
//		node20.setFeuilleDroite(node25);
//		node20.setFeuilleGauche(node5);
//		node5.setFeuilleGauche(node3);
//		node5.setFeuilleDroite(node12);
//		node12.setFeuilleGauche(node8);
//		node12.setFeuilleDroite(node13);
//		node8.setFeuilleGauche(node6);
//		node25.setFeuilleGauche(node21);
//		node25.setFeuilleDroite(node28);
//
//
//
//		//parcoursInfixeBinaryNode(node20);
//
//		// arbre desequilibre
//
////		BinaryNode<Integer> node10 = new BinaryNode<Integer>(10);
////		BinaryNode<Integer> node5 = new BinaryNode<Integer>(5);
////		BinaryNode<Integer> node2 = new BinaryNode<Integer>(2);
////		BinaryNode<Integer> node4 = new BinaryNode<Integer>(4);
////		BinaryNode<Integer> node7 = new BinaryNode<Integer>(7);
////		BinaryNode<Integer> node12 = new BinaryNode<Integer>(12);
////		BinaryNode<Integer> node15 = new BinaryNode<Integer>(15);
////		BinaryNode<Integer> node17 = new BinaryNode<Integer>(17);
//
////		node10.setFeuilleDroite(node12);
////		node10.setFeuilleGauche(node5);
////		node5.setFeuilleDroite(node7);
////		node5.setFeuilleGauche(node2);
////		node2.setFeuilleDroite(node4);
////		node12.setFeuilleDroite(node15);
////		node15.setFeuilleDroite(node17);
//
//		// arbre equilibre
//
//		BinaryNode<Integer> node66 = new BinaryNode<Integer>(66);
//		BinaryNode<Integer> node67 = new BinaryNode<Integer>(67);
//		BinaryNode<Integer> node68 = new BinaryNode<Integer>(68);
//
//
//		node66.setFeuilleGauche(node67);
//		node66.setFeuilleDroite(node68);
//
//
//
//		System.out.println("is equilibre : " + isEquilibre(node66));
//		System.out.println("profon : " + getFacteurEquilibrage(node66));
//
//
//		//huffman
		
		
		
		
		

	}
	
	
}
