package main;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class Node<T> {
    public List<Node<T>> children = new ArrayList<Node<T>>();
    private Node<T> parent = null;
    private T data = null;

    public Node(T data) {
        this.data = data;
    }

    public Node(T data, Node<T> parent) {
        this.data = data;
        this.parent = parent;
    }

    public void setParent(Node<T> parent) {
        this.parent = parent;
    }


    public void addChild(Node<T> child) {
        child.setParent(this);
        this.children.add(child);
    }
    
    public void diplay() {
    	System.out.println(this.data);
    	for (Node<T> children : this.children) {
    		System.out.print(children.data);
    		
    	}
    	System.out.println();
    
    }

	public List<Node<T>> getChildren() {
		return children;
	}

	public void setChildren(List<Node<T>> children) {
		this.children = children;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public Node<T> getParent() {
		return parent;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.data.toString();
	}
	
	
    
    
    

}