package main;

import java.util.List;

public class BinaryNode<T> {

    public BinaryNode(T data) {
        this.data = data;
        this.feuilleDroite = null;
        this.feuilleGauche = null;
    }

    public BinaryNode<T> feuilleDroite;
    public BinaryNode<T> feuilleGauche;
    public T data = null;

    public BinaryNode<T> getFeuilleDroite() {
        return feuilleDroite;
    }

    public void setFeuilleDroite(BinaryNode<T> feuilleDroite) {
        this.feuilleDroite = feuilleDroite;
    }

    public BinaryNode<T> getFeuilleGauche() {
        return feuilleGauche;
    }

    public void setFeuilleGauche(BinaryNode<T> feuilleGauche) {
        this.feuilleGauche = feuilleGauche;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

//    public void insert(BinaryNode<Integer> node, int value) {
//        if (value < node.data) {
//            if (node.feuilleGauche != null) {
//                insert(node.feuilleDroite, value);
//            }
//        }
//    }

    public void insert(BinaryNode<Integer> node, int value) {
        if (value < node.data) {
            if (node.feuilleGauche != null) {
                insert(node.feuilleGauche, value);
            } else {
                node.feuilleGauche = new BinaryNode<>(value);
            }
        } else if (value > node.data) {
            if (node.feuilleDroite != null) {
                insert(node.feuilleDroite, value);
            } else {
                node.feuilleDroite = new BinaryNode<>(value);
            }
        }
    }

    public void createFromList(BinaryNode<Integer> node, List<Integer> values) {
        for(Integer value : values) {
            node.insert(node, value);
        }
    }

    public BinaryNode<Integer> search(BinaryNode<Integer> node, Integer valueSerched) {
        if (node == null || node.data == valueSerched) {
            return node;
        }

        if (node.data > valueSerched) {
            return search(node.feuilleGauche, valueSerched);
        }

        return search(node.feuilleDroite, valueSerched);
    }


}
